<?php

namespace Drupal\opquast_checklist\Service;

use Drupal\Core\Language\LanguageManagerInterface;

/**
 * Service for interacting with Opquast website.
 */
class OpquastService {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private LanguageManagerInterface $languageManager;

  /**
   * Constructor for OpquastService.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The LanguageManagerInterface object.
   */
  public function __construct(LanguageManagerInterface $languageManager) {
    $this->languageManager = $languageManager;
  }

  /**
   * Detect the current language and return the base path of rules.
   *
   *  If it does not exist, returns the English language.
   *
   * @param string $path
   *   The opquast checklist path.
   *
   * @return string
   *   The base URL of rules.
   */
  public function getBasePath(string $path = ''): string {
    $basesPaths = [
      'fr' => 'fr/assurance-qualite-web',
      'en' => 'en/web-quality-assurance',
    ];
    $language = $this->languageManager->getCurrentLanguage()->getId();
    $base = $basesPaths['en'];
    if (in_array($language, array_keys($basesPaths))) {
      $base = $basesPaths[$language];
    }

    return "{$this->getApiEndPoint()}$base/$path";
  }

  /**
   * Get the license path of the opquast website.
   *
   * @return string
   *   The URL for the Opquast website's license path.
   */
  public function getLicencePath(): string {
    return "{$this->getBasePath('licence')}/";
  }

  /**
   * Get endpoint url of opquast website.
   *
   * @return string
   *   The endpoint URL of opquast website.
   */
  public function getApiEndPoint(): string {
    return 'https://checklists.opquast.com/';
  }

}
