(function ($) {
  /**
   * Adds dynamic custom item descriptions toggling.
   */
  Drupal.behaviors.opquast_checklistDisplayGoals = {
    attach(context) {
      const isCompactMode = $('#checklistapi-checklist-form', context).hasClass(
        'compact-mode',
      );
      const text = isCompactMode
        ? Drupal.t('Show goals')
        : Drupal.t('Hide goals');
      $('#checklistapi-checklist-form .compact-goals', context).html(
        `<a href="#">${text}</a>`,
      );
      $('#checklistapi-checklist-form .compact-goals a', context).click(
        function () {
          $(this)
            .closest('#checklistapi-checklist-form')
            .toggleClass('compact-mode');
          const isCompactMode = $(this)
            .closest('#checklistapi-checklist-form')
            .hasClass('compact-mode');
          $(this).get(0).textContent = isCompactMode
            ? Drupal.t('Show goals')
            : Drupal.t('Hide goals');
          $(this).attr(
            'title',
            isCompactMode
              ? Drupal.t('Expand layout to include goals.')
              : Drupal.t('Compress layout by hiding goals.'),
          );
          $('.form-item__description', context).toggle();
          return false;
        },
      );
    },
  };
})(jQuery);
