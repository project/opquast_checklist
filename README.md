# OPQUAST CHECKLIST

## CONTENTS OF THIS FILE

- [INTRODUCTION](#description)
- [REQUIREMENTS](#requirements)
- [INSTALLATION](#installation)
- [CONFIGURATION](#configuration)
- [MORE INFORMATION](#more-information)

## INTRODUCTION

The [Opquast](https://www.opquast.com/) Checklist module aims to improve the
quality of online services through open-licensed checklists (Creative Commons
BY-SA) and tools validated and used by a large community of web professionals.
The module offers you 226 rules classified by theme:

- Alternatives
- Code
- Contact Information
- Contents
- E-Commerce
- Public spaces
- Files and multimedia
- Forms
- Hyperlinks
- Identification
- Internationalization
- Mobile
- Navigation
- Newsletter
- Presentation
- Security and confidentiality
- Server and Performance
- Syndication
- Tables

## REQUIREMENTS

The module requires
the [Checklist API](https://www.drupal.org/project/checklistapi).

## INSTALLATION

- The Opquast checklist is installed in the usual way.
  See [Installing contributed modules](https://www.drupal.org/documentation/install/modules-themes/modules-8).

## CONFIGURATION

- To start using the module, go to Reports > Checklists >  Opquast Checklist
  or `admin/reports/checklistapi/opquast`.
- From this page you can check the rules you have implemented on your project.
  Remember to save your progress before leaving the page.

## MORE INFORMATION

- The module is available in English and French versions.
- Possibility to hide or show objectives
- Don't understand a rules or don't know how to implement it? No problem click
  on its link,
  you will be redirected to our where you will find all the information you need
  to set it up.
- Opquast website  :  https://www.opquast.com/
